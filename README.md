## Requirements

- IDE Setup: [WebStorm](https://www.jetbrains.com/webstorm/) or [IntelliJ Idea](https://www.jetbrains.com/idea/)

- Package manager: [NPM](https://www.npmjs.com/)

- Configuration: [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm dev
```

### Compile and Minify for Production

```sh
npm build
```

### build docker image

```
docker build -f cicd/build/Dockerfile .
```
