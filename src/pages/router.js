import {createRouter, createWebHistory} from "vue-router";
import profileRoutes from "./profile/router";
import gamePlanRoutes from "./gameplan/router";
import newGamePlanRoutes from "./gameplan/router";
import playgroundRoutes from "./playground/router";
import pageDesignRoutes from "./pagedesign/router";
import HomePage from "./HomePage.vue";
import ContactPage from "./ContactPage.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: HomePage,
  },
  {
    path: "/contact",
    name: "contact",
    component: ContactPage,
  },
  ...profileRoutes,
  ...gamePlanRoutes,
  ...newGamePlanRoutes,
  ...playgroundRoutes,
  ...pageDesignRoutes,
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

export default router;
