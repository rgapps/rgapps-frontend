import GamePlanPage from "@/pages/gameplan/GamePlanPage.vue";
import GalleryPage from "@/pages/gameplan/GalleryPage.vue";
import AdminPage from "@/pages/gameplan/AdminPage.vue";
import StorePage from "@/pages/gameplan/StorePage.vue";
import AboutPage from "@/pages/gameplan/AboutPage.vue";

const routes = [
  {
    path: "/gameplan",
    name: "GamePlan",
    component: GamePlanPage,
  },
  {
    path: "/gameplan/gallery",
    name: "GalleryPage",
    component: GalleryPage,
  },
  {
    path: "/gameplan/admin",
    name: "AdminPage",
    component: AdminPage,
  },
  {
    path: "/gameplan/store",
    name: "StorePage",
    component: StorePage,
  },
  {
    path: "/gameplan/about",
    name: "AboutPage",
    component: AboutPage,
  },
];

export default routes;
