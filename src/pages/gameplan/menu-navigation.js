export const userNavigation = {
  "/gameplan/*": [
    {text: "Home", to: "/gameplan"},
    {text: "Gallery", to: "/gameplan/gallery"},
    {text: "store", to: "/gameplan/store"},
    {text: "About us", to: "/gameplan/about"},
  ],
};

export const socialLinks = {
  "/gameplan/*": [
    {
      icon: "mdi-instagram",
      link: "",
      color: "purple accent-4",
    },
    {
      icon: "mdi-facebook",
      link: "",
      color: "blue-darken-2",
    },
  ],
};
