import PageDesignPage from "@/pages/pagedesign/PageDesignPage.vue";

const routes = [
  {
    path: "/design",
    name: "design",
    component: PageDesignPage,
  },
];

export default routes;
