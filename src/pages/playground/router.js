import PlaygroundPage from "@/pages/playground/PlaygroundPage.vue";

const routes = [
  {
    path: "/playground",
    name: "Playground",
    component: PlaygroundPage,
  },
];

export default routes;
