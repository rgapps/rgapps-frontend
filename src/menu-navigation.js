import {userNavigation as gameplanNavigation} from "@/pages/gameplan/menu-navigation";
import {socialLinks as gameplanSocialLinks} from "@/pages/gameplan/menu-navigation";

export const userNavigation = {
  ...gameplanNavigation,
};

export const socialLinks = {
  ...gameplanSocialLinks,
};
