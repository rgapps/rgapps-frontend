import useHttpGet from "@/common/hooks/http/useHttpGet";
import useHttpPost from "@/common/hooks/http/useHttpPost";
import useHttpDelete from "@/common/hooks/http/useHttpDelete";
import useHttpPut from "@/common/hooks/http/useHttpPut";
import {computed} from "vue";

export const crudService = (uri) => {
  const {
    request: retrieve,
    response: retrieved,
    error: errorRetrieving,
    loading: retrieving,
    status: retrieveStatus,
  } = useHttpGet(uri);
  const {
    request: create,
    response: created,
    error: errorCreating,
    loading: creating,
    status: createStatus,
  } = useHttpPost(uri);
  const {
    request: update,
    response: updated,
    error: errorUpdating,
    loading: updating,
    status: updateStatus,
  } = useHttpPut(uri);
  const {
    request: remove,
    response: deleted,
    error: errorDeleting,
    loading: deleting,
    status: deleteStatus,
  } = useHttpDelete(uri);

  return {
    retrieve,
    create,
    update,
    remove,

    retrieved,
    created,
    updated,
    deleted,
    retrieving,
    creating,
    updating,
    deleting,
    errorRetrieving,
    errorCreating,
    errorUpdating,
    errorDeleting,
    retrieveStatus,
    createStatus,
    updateStatus,
    deleteStatus,

    error: computed(
      () =>
        errorRetrieving.value || errorCreating.value || errorUpdating.value || errorDeleting.value
    ),
    saving: computed(() => creating.value || updating.value || deleting.value),
  };
};
