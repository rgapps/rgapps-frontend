import {ref} from "vue";
import {slackUri} from "@/environment";

export const sendMessage = () => {
  const messageSent = ref();
  const loading = ref(false);

  const send = ({email, contact, message}) => {
    const text = `
      *CONTACT REQUEST*\n
      \t*Email:* ${email}\n
      \t*Contact No:* ${contact}\n
      \t*Message:*\n
      \t${message}
    `;

    loading.value = true;
    fetch(slackUri, {
      method: "POST",
      body: JSON.stringify({text}),
    })
      .then(async (res) => {
        messageSent.value = await res.json();
      })
      .finally(() => {
        loading.value = false;
      });
  };

  return {messageSent, loading, send};
};
