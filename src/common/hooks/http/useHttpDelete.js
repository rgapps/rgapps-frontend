import {ref} from "vue";

const useHttpDelete = (uri) => {
  const response = ref(null);
  const error = ref(null);
  const loading = ref(false);
  const status = ref(0);

  const request = (id) => {
    loading.value = true;
    fetch(`${uri}/${id}`, {
      method: "DELETE",
    })
      .then((res) => {
        response.value = res;
        status.value = res.status;
      })
      .catch((err) => {
        error.value = err;
        if (err.response) status.value = err.response.status;
      })
      .finally(() => {
        loading.value = false;
      });
  };

  return {response, error, loading, request, status};
};

export default useHttpDelete;
