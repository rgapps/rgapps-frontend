import {ref} from "vue";

const useHttpPut = (uri) => {
  const response = ref(null);
  const error = ref(null);
  const loading = ref(false);
  const status = ref(0);

  const request = (id, body) => {
    loading.value = true;
    fetch(`${uri}/${id}`, {
      method: "PUT",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(body),
    })
      .then(async (res) => {
        response.value = await res.json();
        status.value = res.status;
      })
      .catch((err) => {
        error.value = err;
        if (err.response) status.value = err.response.value;
      })
      .finally(() => {
        loading.value = false;
      });
  };

  return {response, error, loading, request, status};
};

export default useHttpPut;
