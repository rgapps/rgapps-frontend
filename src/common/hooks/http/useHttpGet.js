import {ref} from "vue";

const useHttpGet = (uri) => {
  const response = ref(null);
  const error = ref(null);
  const loading = ref(false);
  const status = ref(0);

  const request = () => {
    loading.value = true;
    fetch(`${uri}`, {
      method: "GET",
    })
      .then((res) => {
        status.value = res.status;
        return res.json();
      })
      .then((resBody) => {
        response.value = resBody;
      })
      .catch((err) => {
        error.value = err;
        if (err.response) status.value = err.response.status;
      })
      .finally(() => {
        loading.value = false;
      });
  };

  return {response, status, error, loading, request};
};

export default useHttpGet;
