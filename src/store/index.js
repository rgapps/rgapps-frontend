import {createPinia} from "pinia";
import useGamesStore from "@/store/gameStore";
import useShoppingStore from "@/store/shoppingStore";

const pinia = createPinia();

export {useGamesStore, useShoppingStore};

export default pinia;
