import {defineStore} from "pinia";
import {ref} from "vue";
import gameService from "@/services/gameService";

const useGamesStore = defineStore("games", () => {
  const games = ref([]);
  const isConnected = ref(false);
  const editingGame = ref({
    state: false,
    game: {
      imageUrl: "",
      title: "",
    },
  });

  const setGames = (newGames) => {
    games.value = newGames;
  };

  const editGame = (game) => {
    editingGame.value.state = !editingGame.value.state;
    editingGame.value.game = game;
  };

  const getGames = () => {
    const eventSource = gameService.streams.getGames();
    eventSource.onopen = () => (isConnected.value = true);

    eventSource.onmessage = ({data}) => {
      const parsed = JSON.parse(data);
      setGames(parsed);
    };
  };

  return {
    games,
    isConnected,
    editingGame,
    getGames,
    setGames,
    editGame,
  };
});

export default useGamesStore;
