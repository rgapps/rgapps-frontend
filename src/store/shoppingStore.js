import {defineStore} from "pinia";
import {computed, ref} from "vue";

const useShoppingStore = defineStore("shopping", () => {
  const shoppingCart = ref([]);
  const showingShoppingCart = ref(false);

  const cartInformation = computed(() => {
    let price = 0;
    let products = 0;

    shoppingCart.value.forEach((product) => {
      price += product.price * product.quantity;
      products += product.quantity;
    });

    return {price, products};
  });

  const toggleCart = () => {
    showingShoppingCart.value = !showingShoppingCart.value;
  };

  const addProduct = (product) => {
    const index = shoppingCart.value.findIndex((e) => product.id === e.id);

    if (index === -1) {
      shoppingCart.value.push(product);
      return;
    }

    shoppingCart.value[index].quantity += 1;
  };

  const removeProduct = ({product, all}) => {
    const index = shoppingCart.value.findIndex((e) => product.id === e.id);

    const hasMultiple = shoppingCart.value[index].quantity > 1;

    if (hasMultiple && !all) {
      shoppingCart.value[index].quantity -= 1;
      return;
    }

    shoppingCart.value.splice(index, 1);
  };

  return {
    shoppingCart,
    cartInformation,
    showingShoppingCart,
    toggleCart,
    addProduct,
    removeProduct,
  };
});

export default useShoppingStore;
