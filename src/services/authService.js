import {computed, ref} from "vue";

export const useAuthentication = () => {
  const isAuthenticated = ref(false);

  const user = ref({
    name: "John Doe",
    nickname: "iamjohndoe",
    admin: true,
  });

  const isAdmin = computed(() => user.value.admin);

  const login = () => {
    isAuthenticated.value = true;
  };

  const logout = () => {
    isAuthenticated.value = false;
  };

  return {
    user,
    isAuthenticated,
    isAdmin,
    login,
    logout,
  };
};
