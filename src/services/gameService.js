import axios from "axios";
import {backendUri} from "@/environment";

const backendUrl = `${backendUri}`;
let service = axios.create({baseURL: `${backendUrl}`});

const endpoints = {
  games: `/games`,
  gamesStream: `/games/stream`,
  game: (id) => `/games/${id}`,
};

const gameService = {
  async addGame(game, token) {
    return service.post(endpoints.games, game, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },

  async updateGame(id, game, token) {
    return service.put(endpoints.game(id), game, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },

  async deleteGame(id, token) {
    return service.delete(endpoints.game(id), {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },

  streams: {
    getGames: function () {
      return new EventSource(`${backendUrl}${endpoints.gamesStream}`);
    },
  },
};

export default gameService;
